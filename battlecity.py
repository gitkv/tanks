import pygame
import sys
import random

pygame.init()

WIDTH, HEIGHT = 800, 800
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Battle City")

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
WALL_COLOR = (169, 169, 169)
EAGLE_COLOR = (255, 215, 0)
TANK_SIZE = 40
TANK_SPEED = 4
ENEMY_SPEED = 2
BULLET_SIZE = 5
BULLET_SPEED = 8
WALL_SIZE = 40
PLAYER_COLOR = (0, 255, 0)
ENEMY_COLOR = (255, 0, 0)
PLAYER_LIVES = 3
NUM_ENEMIES = 20
ENEMY_CHANGE_DIRECTION_DELAY = 60

player = pygame.Rect(WIDTH // 2 - TANK_SIZE // 2, HEIGHT - TANK_SIZE * 2, TANK_SIZE, TANK_SIZE)
eagle = pygame.Rect(WIDTH // 2 - WALL_SIZE // 2, HEIGHT - WALL_SIZE, WALL_SIZE, WALL_SIZE)
player_bullets = []
enemy_bullets = []
walls = [pygame.Rect(i * WALL_SIZE, HEIGHT // 2, WALL_SIZE, WALL_SIZE) for i in range(WIDTH // WALL_SIZE)]

clock = pygame.time.Clock()
enemy_shoot_timer = 0

player_direction = "up"

enemy_list = []
for _ in range(NUM_ENEMIES):
    x = random.randint(1, WIDTH // WALL_SIZE - 2) * WALL_SIZE
    y = random.randint(1, HEIGHT // WALL_SIZE - 8) * WALL_SIZE
    enemy_rect = pygame.Rect(x, y, TANK_SIZE, TANK_SIZE)
    enemy = {"rect": enemy_rect, "direction": random.choice(["left", "right", "up", "down"]), "timer": ENEMY_CHANGE_DIRECTION_DELAY}
    enemy_list.append(enemy)

def draw_tank(rect, color, direction):
    pygame.draw.rect(screen, color, rect)

    barrel_width = rect.width // 4
    barrel_height = rect.height // 4

    if direction == "left":
        barrel = pygame.Rect(rect.left - barrel_height, rect.centery - barrel_width // 2, barrel_height, barrel_width)
    elif direction == "right":
        barrel = pygame.Rect(rect.right, rect.centery - barrel_width // 2, barrel_height, barrel_width)
    elif direction == "up":
        barrel = pygame.Rect(rect.centerx - barrel_width // 2, rect.top - barrel_height, barrel_width, barrel_height)
    elif direction == "down":
        barrel = pygame.Rect(rect.centerx - barrel_width // 2, rect.bottom, barrel_width, barrel_height)

    pygame.draw.rect(screen, color, barrel)


# Создаем функцию для генерации уровня
def generate_level(level):
    global walls, enemies, player, eagle
    level_data = LEVELS[level % len(LEVELS)]
    walls = []
    for y, row in enumerate(level_data):
        for x, cell in enumerate(row):
            if cell == "W":
                wall = pygame.Rect(x * WALL_SIZE, y * WALL_SIZE, WALL_SIZE, WALL_SIZE)
                walls.append(wall)

    enemies = []
    for _ in range(NUM_ENEMIES):
        x = random.randint(1, WIDTH // WALL_SIZE - 2) * WALL_SIZE
        y = random.randint(1, HEIGHT // WALL_SIZE - 8) * WALL_SIZE
        enemy = pygame.Rect(x, y, WALL_SIZE, WALL_SIZE)
        enemies.append(enemy)
    player = pygame.Rect(WIDTH // 2 - WALL_SIZE // 2, HEIGHT - 3 * WALL_SIZE, WALL_SIZE, WALL_SIZE)
    eagle = pygame.Rect(WIDTH // 2 - WALL_SIZE // 2, HEIGHT - 2 * WALL_SIZE, WALL_SIZE, WALL_SIZE)


def shoot_bullet(player, player_direction):
    bullet = pygame.Rect(player.centerx - BULLET_SIZE // 2, player.centery - BULLET_SIZE // 2, BULLET_SIZE, BULLET_SIZE)
    player_bullets.append({"rect": bullet, "direction": player_direction})

def move_enemies(enemy_list):
    for enemy in enemy_list:
        rect = enemy["rect"]
        direction = enemy["direction"]
        enemy["timer"] -= 1

        if enemy["timer"] <= 0:
            enemy["direction"] = random.choice(["left", "right", "up", "down"])
            enemy["timer"] = ENEMY_CHANGE_DIRECTION_DELAY

        if direction == "left":
            new_x = rect.x - ENEMY_SPEED
            rect_copy = rect.copy()
            rect_copy.x = new_x
            if not (check_collision(rect_copy, walls) or rect_copy.colliderect(player) or check_collision(rect_copy, [other_enemy["rect"] for other_enemy in enemy_list if other_enemy != enemy])):
                rect.x = new_x
        elif direction == "right":
            new_x = rect.x + ENEMY_SPEED
            rect_copy = rect.copy()
            rect_copy.x = new_x
            if not (check_collision(rect_copy, walls) or rect_copy.colliderect(player) or check_collision(rect_copy, [other_enemy["rect"] for other_enemy in enemy_list if other_enemy != enemy])):
                rect.x = new_x
        elif direction == "up":
            new_y = rect.y - ENEMY_SPEED
            rect_copy = rect.copy()
            rect_copy.y = new_y
            if not (check_collision(rect_copy, walls) or rect_copy.colliderect(player) or check_collision(rect_copy, [other_enemy["rect"] for other_enemy in enemy_list if other_enemy != enemy])):
                rect.y = new_y
        elif direction == "down":
            new_y = rect.y + ENEMY_SPEED
            rect_copy = rect.copy()
            rect_copy.y = new_y
            if not (check_collision(rect_copy, walls) or rect_copy.colliderect(player) or check_collision(rect_copy, [other_enemy["rect"] for other_enemy in enemy_list if other_enemy != enemy])):
                rect.y = new_y

        rect.x = max(0, min(rect.x, WIDTH - TANK_SIZE))
        rect.y = max(0, min(rect.y, HEIGHT - TANK_SIZE))

def check_collision(rect, rect_list):
    for other_rect in rect_list:
        if rect.colliderect(other_rect):
            return True
    return False

def game_over_screen():
    screen.fill(BLACK)
    font = pygame.font.Font(None, 36)
    text = font.render("Game Over", True, WHITE)
    text_rect = text.get_rect()
    text_rect.center = (WIDTH // 2, HEIGHT // 2)
    screen.blit(text, text_rect)
    pygame.display.flip()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                return

def check_bullet_collisions(bullets, objects):
    collisions = []
    for i, bullet in enumerate(bullets):
        for obj in objects:
            if bullet.colliderect(obj):
                collisions.append((i, obj))
    return collisions

def update_bullet_positions(bullets, direction, speed):
    for bullet in bullets:
        if direction == "left":
            bullet.x -= speed
        elif direction == "right":
            bullet.x += speed
        elif direction == "up":
            bullet.y -= speed
        elif direction == "down":
            bullet.y += speed

def draw_bullets(bullets, color):
    for bullet in bullets:
        pygame.draw.rect(screen, color, bullet)

LEVELS = [
    [
        "WWWWWWWWWWWWWWWWWWWW",
        "W                  W",
        "W                  W",
        "W       WWWWW      W",
        "W  WWW      WW     W",
        "W           W      W",
        "W               WWWW",
        "W        WWW       W",
        "W                  W",
        "W           W      W",
        "W    WWW           W",
        "W              WWWWW",
        "W       W          W",
        "WWWW    W   WWWW   W",
        "W       W          W",
        "W       w     W    W",
        "W             W    W",
        "W             W    W",
        "W   WWW W     W    W",
        "WWWWWWWWWWWWWWWWWWWW"
    ],
]

current_level = 0
generate_level(current_level)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                shoot_bullet(player, player_direction)

    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        new_x = player.x - TANK_SPEED
        player_copy = player.copy()
        player_copy.x = new_x
        if not (check_collision(player_copy, walls) or check_collision(player_copy, [enemy["rect"] for enemy in enemy_list])):
            player.x = new_x
        player_direction = "left"
    if keys[pygame.K_RIGHT]:
        new_x = player.x + TANK_SPEED
        player_copy = player.copy()
        player_copy.x = new_x
        if not (check_collision(player_copy, walls) or check_collision(player_copy, [enemy["rect"] for enemy in enemy_list])):
            player.x = new_x
        player_direction = "right"
    if keys[pygame.K_UP]:
        new_y = player.y - TANK_SPEED
        player_copy = player.copy()
        player_copy.y = new_y
        if not (check_collision(player_copy, walls) or check_collision(player_copy, [enemy["rect"] for enemy in enemy_list])):
            player.y = new_y
        player_direction = "up"
    if keys[pygame.K_DOWN]:
        new_y = player.y + TANK_SPEED
        player_copy = player.copy()
        player_copy.y = new_y
        if not (check_collision(player_copy, walls) or check_collision(player_copy, [enemy["rect"] for enemy in enemy_list])):
            player.y = new_y
        player_direction = "down"

    player.x = max(0, min(player.x, WIDTH - TANK_SIZE))
    player.y = max(0, min(player.y, HEIGHT - TANK_SIZE))

    move_enemies(enemy_list)

    if enemy_shoot_timer <= 0:
        random_enemy = random.choice(enemies)
        enemy_bullet = pygame.Rect(random_enemy.centerx - BULLET_SIZE // 2, random_enemy.y + TANK_SIZE, BULLET_SIZE, BULLET_SIZE)
        enemy_bullets.append(enemy_bullet)
        enemy_shoot_timer = 60
    else:
        enemy_shoot_timer -= 1

    for i in range(len(player_bullets)):
        bullet = player_bullets[i]['rect']
        direction = player_bullets[i]['direction']

        if direction == "left":
            bullet.x -= BULLET_SPEED
        elif direction == "right":
            bullet.x += BULLET_SPEED
        elif direction == "up":
            bullet.y -= BULLET_SPEED
        elif direction == "down":
            bullet.y += BULLET_SPEED

    i = 0
    while i < len(player_bullets):
        bullet_info = player_bullets[i]
        bullet = bullet_info['rect']
        wall_hit = None
        for wall in walls:
            if bullet.colliderect(wall):
                wall_hit = wall
                break

        enemy_hit = None
        for enemy in enemy_list:
            if bullet.colliderect(enemy["rect"]):
                enemy_hit = enemy
                break

        if wall_hit:
            walls.remove(wall_hit)
            player_bullets.pop(i)
        elif enemy_hit:
            enemy_list.remove(enemy_hit)
            player_bullets.pop(i)
        else:
            pygame.draw.rect(screen, WHITE, bullet)
            i += 1

    i = 0
    while i < len(enemy_bullets):
        bullet = enemy_bullets[i]
        bullet.y += BULLET_SPEED

        wall_hit = None
        for wall in walls:
            if bullet.colliderect(wall):
                wall_hit = wall
                break

        enemy_hit = None
        for enemy in enemy_list:
            if bullet.colliderect(enemy["rect"]):
                enemy_hit = enemy
                break

        if bullet.colliderect(player):
            PLAYER_LIVES -= 1
            if PLAYER_LIVES <= 0:
                game_over_screen()
            enemy_bullets.pop(i)
        elif bullet.colliderect(eagle):
            game_over_screen()
        elif wall_hit:
            walls.remove(wall_hit)
            enemy_bullets.pop(i)
        elif enemy_hit:
            enemy_list.remove(enemy_hit)
            enemy_bullets.pop(i)
        else:
            pygame.draw.rect(screen, WHITE, bullet)
            i += 1

    screen.fill(BLACK)
    draw_tank(player, PLAYER_COLOR, player_direction)
    pygame.draw.rect(screen, EAGLE_COLOR, eagle)
    for enemy in enemy_list:
        rect = enemy["rect"]
        direction = enemy["direction"]
        draw_tank(rect, ENEMY_COLOR, direction)
    for wall in walls:
        pygame.draw.rect(screen, WALL_COLOR, wall)
    for bullet_info in player_bullets:
        bullet = bullet_info['rect']
        pygame.draw.rect(screen, WHITE, bullet)
    for bullet in enemy_bullets:
        pygame.draw.rect(screen, WHITE, bullet)


    if len(enemies) == 0:
        current_level += 1
        generate_level(current_level)
    
    if PLAYER_LIVES <= 0:
        game_over_screen()
        PLAYER_LIVES = 3
        generate_level(current_level)

    if bullet.colliderect(eagle):
        game_over_screen()
        PLAYER_LIVES = 3
        generate_level(current_level)


    pygame.display.flip()
    clock.tick(60)

