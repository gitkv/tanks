import pygame
import random
from constants import *
from game_objects import Tank, Bullet, Wall, Base, EnemyState, CollisionType, Explosion
from levels import LEVELS

PLAYER_START_POS = None

def handle_input(tank, bullets, walls, base):
    keys = pygame.key.get_pressed()
    
    dx, dy = 0, 0
    if keys[pygame.K_LEFT] or keys[pygame.K_a]:
        tank.direction = "left"
        dx = -TANK_SPEED * 0.5
    elif keys[pygame.K_RIGHT] or keys[pygame.K_d]:
        tank.direction = "right"
        dx = TANK_SPEED * 0.5
    elif keys[pygame.K_UP] or keys[pygame.K_w]:
        tank.direction = "up"
        dy = -TANK_SPEED * 0.5
    elif keys[pygame.K_DOWN] or keys[pygame.K_s]:
        tank.direction = "down"
        dy = TANK_SPEED * 0.5

    tank.move(dx, dy, walls, base, [])

    keep_tank_in_bounds(tank)

    current_time = pygame.time.get_ticks()
    if keys[pygame.K_SPACE] and current_time - tank.last_fire_time >= FIRE_DELAY:
        tank.last_fire_time = current_time
        bullet_x = tank.rect.centerx - BULLET_SIZE // 2
        bullet_y = tank.rect.centery - BULLET_SIZE // 2
        bullet = Bullet(bullet_x, bullet_y, tank.direction, tank)  # Передаем tank в качестве owner
        bullets.append(bullet)

def check_collisions(tank, bullets, enemies, walls, base, explosions):
    for i, bullet in enumerate(bullets):
        bullet_hit = False

        for j, enemy in enumerate(enemies):
            if bullet.owner != enemy and bullet.rect.colliderect(enemy.rect) and bullet.owner == tank:
                bullet_hit = True
                explosion = Explosion(bullet.rect.centerx, bullet.rect.centery, EXPLOSION_SIZE)
                explosions.append(explosion)
                del enemies[j]
                return CollisionType.ENEMY_KILLED

        for k, wall in enumerate(walls):
            if bullet.rect.colliderect(wall.rect):
                wall.lives -= 1
                bullet_hit = True
                if wall.lives == 0:
                    del walls[k]
                break

        if bullet.rect.colliderect(base.rect):
            bullet_hit = True
            return CollisionType.GAME_OVER

        if bullet.owner != tank and bullet.rect.colliderect(tank.rect) and tank.invulnerable_until < pygame.time.get_ticks():
            tank.lives -= 1
            bullet_hit = True
            explosion = Explosion(tank.rect.x, tank.rect.y, EXPLOSION_SIZE)
            explosions.append(explosion)
            if tank.lives > 0:
                tank.rect.x, tank.rect.y = PLAYER_START_POS
                tank.invulnerable_until = pygame.time.get_ticks() + 3000
                return CollisionType.PLAYER_HIT
            else:
                return CollisionType.GAME_OVER

        if bullet_hit:
            del bullets[i]
            explosions.append(Explosion(bullet.rect.centerx, bullet.rect.centery, EXPLOSION_SIZE))

    for i, bullet1 in enumerate(bullets):
        for j, bullet2 in enumerate(bullets[i + 1:]):
            if bullet1.rect.colliderect(bullet2.rect) and bullet1.owner != bullet2.owner:
                # Удаляем оба снаряда и добавляем взрыв
                explosions.append(Explosion(bullet1.rect.centerx, bullet1.rect.centery, EXPLOSION_SIZE*1.5))
                del bullets[i]
                del bullets[j]
                break
            
    for enemy1 in enemies:
        for enemy2 in enemies:
            if enemy1 != enemy2 and enemy1.rect.colliderect(enemy2.rect):
                enemy1.state = random.choice([EnemyState.IDLE, EnemyState.ATTACK_BASE, EnemyState.AVOID_PLAYER, EnemyState.ATTACK_PLAYER])
                enemy2.state = random.choice([EnemyState.IDLE, EnemyState.ATTACK_BASE, EnemyState.AVOID_PLAYER, EnemyState.ATTACK_PLAYER])

    for enemy in enemies:
        if tank.rect.colliderect(enemy.rect) and tank.invulnerable_until < pygame.time.get_ticks():
            tank.lives -= 1
            if tank.lives > 0:
                tank.rect.x, tank.rect.y = PLAYER_START_POS
                tank.invulnerable_until = pygame.time.get_ticks() + 3000
                return CollisionType.PLAYER_HIT
            else:
                return CollisionType.GAME_OVER

    return CollisionType.NONE


def keep_tank_in_bounds(tank):
    if tank.rect.x < 0:
        tank.rect.x = 0
    elif tank.rect.x + tank.rect.width > WIDTH:
        tank.rect.x = WIDTH - tank.rect.width

    if tank.rect.y < 0:
        tank.rect.y = 0
    elif tank.rect.y + tank.rect.height > HEIGHT:
        tank.rect.y = HEIGHT - tank.rect.height

def find_free_space(tank, all_tanks, walls):
    directions = ["left", "right", "up", "down"]
    random.shuffle(directions)

    for direction in directions:
        test_rect = tank.rect.copy()
        if direction == "left":
            test_rect.x -= tank.rect.width
        elif direction == "right":
            test_rect.x += tank.rect.width
        elif direction == "up":
            test_rect.y -= tank.rect.height
        elif direction == "down":
            test_rect.y += tank.rect.height

        collision = False
        for other_tank in all_tanks:
            if other_tank != tank and test_rect.colliderect(other_tank.rect):
                collision = True
                break

        if not collision:
            for wall in walls:
                if test_rect.colliderect(wall.rect):
                    collision = True
                    break

        if not collision:
            return direction

    return None

def enemy_movement(enemies, bullets, walls, player, base):
    for enemy in enemies:
        shoot = False  # Флаг, указывающий, должен ли враг стрелять
        all_tanks = enemies + [player]
        if enemy.state == EnemyState.IDLE:
            if random.random() < 0.05:
                enemy.direction = find_free_space(enemy, all_tanks, walls) or enemy.direction

            if random.random() < 0.05:
                enemy.state = random.choice([EnemyState.CHASE_PLAYER, EnemyState.ATTACK_BASE, EnemyState.AVOID_PLAYER, EnemyState.ATTACK_PLAYER])

        elif enemy.state == EnemyState.CHASE_PLAYER:
            dx, dy = player.rect.x - enemy.rect.x, player.rect.y - enemy.rect.y
            enemy.direction = "right" if dx > 0 else "left" if dx < 0 else "down" if dy > 0 else "up"

            if random.random() < 0.1:
                enemy.state = random.choice([EnemyState.ATTACK_BASE, EnemyState.AVOID_PLAYER, EnemyState.ATTACK_PLAYER])

        elif enemy.state == EnemyState.AVOID_PLAYER:
            dx, dy = player.rect.x - enemy.rect.x, player.rect.y - enemy.rect.y
            enemy.direction = "left" if dx > 0 else "right" if dx < 0 else "up" if dy > 0 else "down"

            if random.random() < 0.1:
                enemy.state = random.choice([EnemyState.CHASE_PLAYER, EnemyState.ATTACK_BASE, EnemyState.ATTACK_PLAYER])

        elif enemy.state == EnemyState.ATTACK_PLAYER:
            dx, dy = player.rect.x - enemy.rect.x, player.rect.y - enemy.rect.y
            enemy.direction = "right" if dx > 0 else "left" if dx < 0 else "down" if dy > 0 else "up"
            
            # Если игрок находится на одной прямой линии с врагом, то враг должен стрелять
            if dx == 0 or dy == 0:
                shoot = True

            if random.random() < 0.05:
                enemy.state = random.choice([EnemyState.IDLE, EnemyState.CHASE_PLAYER, EnemyState.AVOID_PLAYER, EnemyState.ATTACK_BASE])

        elif enemy.state == EnemyState.ATTACK_BASE:
            dx, dy = base.rect.x - enemy.rect.x, base.rect.y - enemy.rect.y
            enemy.direction = "right" if dx > 0 else "left" if dx < 0 else "down" if dy > 0 else "up"

            if random.random() < 0.05:
                enemy.state = random.choice([EnemyState.IDLE, EnemyState.CHASE_PLAYER, EnemyState.AVOID_PLAYER])

        old_x, old_y = enemy.rect.x, enemy.rect.y

        dx, dy = 0, 0
        if enemy.direction == "left":
            dx = -ENEMY_SPEED * 0.5
        elif enemy.direction == "right":
            dx = ENEMY_SPEED * 0.5
        elif enemy.direction == "up":
            dy = -ENEMY_SPEED * 0.5
        elif enemy.direction == "down":
            dy = ENEMY_SPEED * 0.5

        enemy.move(dx, dy, walls, base, [])

        for other_tank in all_tanks:
            if other_tank != enemy and enemy.rect.colliderect(other_tank.rect):
                enemy.rect.x, enemy.rect.y = old_x, old_y
                free_direction = find_free_space(enemy, all_tanks, walls)
                if free_direction:
                    enemy.direction = free_direction
                break

        keep_tank_in_bounds(enemy)

        if (enemy.state == EnemyState.ATTACK_PLAYER and shoot and random.random() < 0.3) or (random.random() < 0.015):
            current_time = pygame.time.get_ticks()
            if current_time - enemy.last_fire_time >= FIRE_DELAY:  # Добавьте проверку на задержку между выстрелами
                enemy.last_fire_time = current_time  # Обновите время последнего выстрела
                bullet_x = enemy.rect.centerx - BULLET_SIZE // 2
                bullet_y = enemy.rect.centery - BULLET_SIZE // 2
                bullet = Bullet(bullet_x, bullet_y, enemy.direction, enemy)  # Передаем enemy в качестве owner
                bullets.append(bullet)

def create_game_objects(level, player_lives):
    global PLAYER_START_POS
    walls = []
    player = None
    base = None
    enemies = []

    for y, row in enumerate(level):
        for x, tile in enumerate(row):
            if tile == "W":
                lives = random.choices([1, 2, 3], weights=[20, 30, 50], k=1)[0]
                walls.append(Wall(x * WALL_SIZE, y * WALL_SIZE, lives=lives))
            elif tile == "P":
                PLAYER_START_POS = (x * WALL_SIZE, y * WALL_SIZE)
                player = Tank(x * WALL_SIZE, y * WALL_SIZE, PLAYER_COLOR, "up", lives=player_lives)
            elif tile == "B":
                base = Base(x * WALL_SIZE, y * WALL_SIZE)
            elif tile == "E":
                enemies.append(Tank(x * WALL_SIZE, y * WALL_SIZE, ENEMY_COLOR, random.choice(["left", "right", "up", "down"])))

    return player, base, enemies, walls
