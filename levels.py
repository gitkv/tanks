import random

LEVELS = [
    [
        "WWWWWWWWWWWWWWWWWWWW",
        "W    W     E       W",
        "W    W             W",
        "W        WWWW      W",
        "W  WWW      WW  E  W",
        "W  E        W      W",
        "WWWW            WWWW",
        "W        WWW       W",
        "W                  W",
        "W      W    W      W",
        "WE   WWW    W      W",
        "W           W  WWWWW",
        "W       W          W",
        "WWWW    W   WWWW   W",
        "W       W          W",
        "WWW     WW    W    W",
        "W             WW   W",
        "W        P         W",
        "W       WWW        W",
        "WWWWWWWWWBWWWWWWWWWW"
    ],
]

def generate_single_level(level_num):
    while True:
        level = []
        
        for y in range(20):
            row = ""
            for x in range(20):
                if y in [0, 19] or x in [0, 19]:
                    row += "W"
                else:
                    row += " "
            level.append(row)
        
        # Генерация внутренних стен
        for _ in range(6 + level_num * 3):  # Увеличение количества стен с уровнем сложности
            wall_length = random.randint(2, 4)
            start_x = random.randint(1, 15)
            start_y = random.randint(1, 15)
            direction = random.choice(["horizontal", "vertical"])
            
            for i in range(wall_length):
                if direction == "horizontal":
                    level[start_y] = level[start_y][:start_x + i] + "W" + level[start_y][start_x + i + 1:]
                else:
                    level[start_y + i] = level[start_y + i][:start_x] + "W" + level[start_y + i][start_x + 1:]

        # Защита базы стенами
        base_x = 9
        base_y = 18
        for i in range(-1, 2):
            level[base_y - 1] = level[base_y - 1][:base_x + i] + "W" + level[base_y - 1][base_x + i + 1:]
            level[base_y + 1] = level[base_y + 1][:base_x + i] + "W" + level[base_y + 1][base_x + i + 1:]
            level[base_y] = level[base_y][:base_x - 1] + "W" + "B" + "W" + level[base_y][base_x + 2:]

        # Расположение игрока
        player_x = 9
        player_y = 16

        level[player_y] = level[player_y][:player_x] + "P" + level[player_y][player_x + 1:]
        
        # Генерация вражеских танков
        for _ in range(2 + level_num):  # Увеличение количества врагов с уровнем сложности
            while True:
                enemy_x = random.randint(1, 18)
                enemy_y = random.randint(1, 9)
                if level[enemy_y][enemy_x] == " " and enemy_x != base_x:
                    level[enemy_y] = level[enemy_y][:enemy_x] + "E" + level[enemy_y][enemy_x + 1:]
                    break
        
        # Проверка наличия игрока и базы на уровне
        player_present = any("P" in row for row in level)
        base_present = any("B" in row for row in level)

        if player_present and base_present:
            break

    return level

def generate_levels(num_levels):
    levels = []
    for level_num in range(num_levels):
        level = generate_single_level(level_num)
        levels.append(level)
    return levels

def print_levels(levels):
    for level_num, level in enumerate(levels):
        print(f"Уровень {level_num + 1}:")
        for row in level:
            print("".join(row))
        print()

LEVELS = generate_levels(30)
# print_levels(LEVELS)