import pygame
from constants import *
from levels import LEVELS
from utils import (
    handle_input, check_collisions, enemy_movement, create_game_objects
)
from game_objects import CollisionType

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption(GAME_NAME)
clock = pygame.time.Clock()

def check_victory(enemies):
    return len(enemies) == 0

def show_message(screen, main_message, sub_message, score=None, level=None):
    def draw_text(text, font, x, y):
        text_surface = font.render(text, True, WHITE)
        screen.blit(text_surface, (x - text_surface.get_width() // 2, y))

    font_big = pygame.font.Font(None, 72)
    font_small = pygame.font.Font(None, 36)

    translucent_background = pygame.Surface((WIDTH, HEIGHT), pygame.SRCALPHA)
    translucent_background.fill((0, 0, 0, 128))
    screen.blit(translucent_background, (0, 0))

    draw_text(GAME_NAME, font_big, WIDTH // 2, HEIGHT // 4)
    draw_text(main_message, font_big, WIDTH // 2, HEIGHT // 2)
    draw_text(sub_message, font_small, WIDTH // 2, HEIGHT // 2 + font_big.get_height() + 20)

    if level is not None:
        draw_text(f"Level: {level}", font_small, WIDTH // 2, HEIGHT // 2 + font_big.get_height() * 2 + 20)

    if score is not None:
        draw_text(f"Score: {score}", font_small, WIDTH // 2, HEIGHT // 2 + font_big.get_height() * 3 + 20)

    pygame.display.flip()


# Отображение счета и уровня на экране
def draw_progress(screen, score, level, player_lives):
    def draw_text_with_background(text, font, x, y):
        text_surface = font.render(text, True, WHITE)
        padding = 5
        background_color = pygame.Color(0, 0, 0, 128)
        background_rect = pygame.Rect(x - padding, y - padding, text_surface.get_width() + 2 * padding, FONT_SIZE + 2 * padding)
        background_surface = pygame.Surface(background_rect.size, pygame.SRCALPHA)
        background_surface.fill(background_color)
        screen.blit(background_surface, background_rect.topleft)
        screen.blit(text_surface, (x, y))

    font = pygame.font.Font(None, FONT_SIZE)
    draw_text_with_background(f"Score: {score}", font, 10, 10)
    draw_text_with_background(f"Level: {level}", font, 10 + font.size(f"Score: {score}")[0] + 10, 10)
    draw_text_with_background(f"Lives: {player_lives}", font, 10 + font.size(f"Score: {score}")[0] + 10 + font.size(f"Level: {level}")[0] + 10, 10)

def main():
    level_index = 0
    score = 0
    player_lives = 5
    player, base, enemies, walls = create_game_objects(LEVELS[level_index], player_lives)
    bullets = []
    explosions = []
    game_over = False
    victory = False
    paused = False

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                paused = not paused

        if not paused:
            handle_input(player, bullets, walls, base)
            collision_result = check_collisions(player, bullets, enemies, walls, base, explosions)
            if collision_result == CollisionType.GAME_OVER:
                game_over = True
            elif collision_result == CollisionType.ENEMY_KILLED:
                score += 1
            elif collision_result == CollisionType.PLAYER_HIT:
                player_lives = player.lives

            enemy_movement(enemies, bullets, walls, player, base)
            victory = check_victory(enemies)

            for bullet in bullets:
                bullet.update()

            screen.fill(BLACK)
            player.draw(screen)
            for enemy in enemies:
                enemy.draw(screen)
            for bullet in bullets:
                bullet.draw(screen)
            for wall in walls:
                wall.draw(screen)
            base.draw(screen)

            # Рисуем взрывы
            for explosion in explosions:
                explosion.draw(screen)

            # Удаляем завершенные взрывы
            # explosions = [explosion for explosion in explosions if not explosion.is_finished()]
            
            draw_progress(screen, score, level_index+1, player.lives)
            pygame.display.flip()
            clock.tick(60)
        else:
            show_message(screen, "PAUSED", "Press ESC to resume", score, level_index+1)

        if game_over:
            show_message(screen, "GAME OVER", "Press any key to restart", score, level_index+1)
        elif victory:
            level_index += 1
            if(player_lives < 8):
                player_lives += 1
            if level_index < len(LEVELS):
                show_message(screen, "Level Victory!", "Press any key to continue", score, level_index+1)
            else:
                show_message(screen, "Game Victory!", "You have completed all levels", score, level_index+1)
                level_index -= 1

        while game_over or victory:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                if event.type == pygame.KEYDOWN:
                    if game_over:
                        level_index = 0
                        score = 0
                    game_over = False
                    victory = False
                    player, base, enemies, walls = create_game_objects(LEVELS[level_index], player_lives)
                    bullets = []

if __name__ == "__main__":
    main()
