README.md
=========

Название игры: FuckinG TankS
----------------------------

FuckinG TankS - это аркадная 2D-игра на Python с использованием библиотеки Pygame. Цель игры - управлять танком, уничтожать врагов и защищать свою базу.

### Требования к системе

* Python 3.6 или новее
* Pygame 1.9.6 или новее

### Установка и запуск

Для начала установите Python и библиотеку Pygame, если они еще не установлены:

#### Установка Python

Загрузите Python с официального сайта: [https://www.python.org/downloads/](https://www.python.org/downloads/)

#### Установка Pygame

Откройте терминал или командную строку и выполните следующую команду:

Copy code

`pip install pygame`

#### Запуск игры

1. Склонируйте или скачайте репозиторий с игрой.
2. Откройте терминал или командную строку и перейдите в каталог с игрой.
3. Запустите игру, выполнив следующую команду:

cssCopy code

`python main.py`

### Геймплей

Управление танком происходит с помощью клавиш со стрелками, а стрельба - с помощью клавиши пробел. Ваша задача - уничтожить все вражеские танки и защитить свою базу. Если база будет уничтожена или ваш танк потеряет все жизни, игра закончится.

Игра состоит из нескольких уровней. Уровень считается пройденным, когда все вражеские танки уничтожены. Если вы прошли все уровни, вы выиграли игру. В любой момент игры можно поставить на паузу, нажав клавишу ESC.

Удачи в игре!

### Автор

Игра была разработана gitkv с помощью GPT-4 от OpenAI.
