import pygame
from constants import *
from enum import Enum

class CollisionType(Enum):
    NONE = 0
    GAME_OVER = 1
    ENEMY_KILLED = 2
    PLAYER_HIT = 3

class EnemyState(Enum):
    IDLE = 1
    CHASE_PLAYER = 2
    ATTACK_BASE = 3
    AVOID_PLAYER = 4
    ATTACK_PLAYER = 5

class Explosion:
    def __init__(self, x, y, size, duration=500):
        self.rect = pygame.Rect(x, y, size, size)
        self.created_time = pygame.time.get_ticks()
        self.duration = duration

    def draw(self, screen):
        elapsed_time = pygame.time.get_ticks() - self.created_time
        progress = elapsed_time / self.duration

        if progress < 0.2:
            color = (255, 255, 255)  # Белый
        elif progress < 0.4:
            color = (255, 255, 0)  # Желтый
        elif progress < 0.6:
            color = (255, 0, 0)  # Красный
        elif progress < 0.8:
            color = (255, 165, 0)  # Оранжевый
        else:
            color = (139, 69, 19)  # Коричневый

        pygame.draw.rect(screen, color, self.rect)

    def is_finished(self):
        return pygame.time.get_ticks() - self.created_time >= self.duration

class Tank:
    def __init__(self, x, y, color, direction, state=EnemyState.IDLE, size=TANK_SIZE * 0.9, lives=1, invulnerable=False, explosion_duration=500):
        self.rect = pygame.Rect(x, y, size, size)
        self.color = color
        self.direction = direction
        self.state = state
        self.last_fire_time = 0
        self.lives = lives
        self.invulnerable_until = 0 if not invulnerable else pygame.time.get_ticks() + 3000

    def draw(self, screen):
        if self.invulnerable_until > pygame.time.get_ticks():
            # Мигание при неуязвимости
            if pygame.time.get_ticks() % 200 < 100:
                return
        pygame.draw.rect(screen, self.color, self.rect)
        self.draw_barrel(screen)

    def draw_barrel(self, screen):
        barrel_width = self.rect.width // 4
        barrel_height = self.rect.height // 4
        barrel_directions = {
            "left": pygame.Rect(self.rect.left - barrel_height, self.rect.centery - barrel_width // 2, barrel_height, barrel_width),
            "right": pygame.Rect(self.rect.right, self.rect.centery - barrel_width // 2, barrel_height, barrel_width),
            "up": pygame.Rect(self.rect.centerx - barrel_width // 2, self.rect.top - barrel_height, barrel_width, barrel_height),
            "down": pygame.Rect(self.rect.centerx - barrel_width // 2, self.rect.bottom, barrel_width, barrel_height)
        }
        barrel = barrel_directions[self.direction]
        pygame.draw.rect(screen, self.color, barrel)

    def move(self, dx, dy, walls, base, other_tanks):
        old_x, old_y = self.rect.x, self.rect.y
        self.rect.x += dx
        self.rect.y += dy

        for wall in walls:
            if self.rect.colliderect(wall.rect):
                self.rect.x, self.rect.y = old_x, old_y
                return

        if self.rect.colliderect(base.rect):
            self.rect.x, self.rect.y = old_x, old_y
            return

        for other_tank in other_tanks:
            if self != other_tank and self.rect.colliderect(other_tank.rect):
                self.rect.x, self.rect.y = old_x, old_y
                return

class Bullet:
    def __init__(self, x, y, direction, owner, size=BULLET_SIZE):
        self.rect = pygame.Rect(x, y, size, size)
        self.direction = direction
        self.owner = owner

    def update(self):
        bullet_directions = {
            "left": (-BULLET_SPEED, 0),
            "right": (BULLET_SPEED, 0),
            "up": (0, -BULLET_SPEED),
            "down": (0, BULLET_SPEED)
        }
        self.rect.move_ip(*bullet_directions[self.direction])

    def draw(self, screen):
        pygame.draw.rect(screen, WHITE, self.rect)

class Wall:
    def __init__(self, x, y, size=WALL_SIZE, lives=3):
        self.rect = pygame.Rect(x, y, size, size)
        self.lives = lives

    def draw(self, screen):
        color_intensity = 32 * (3 - self.lives)
        color = (max(WALL_COLOR[0] - color_intensity, 0),
                 max(WALL_COLOR[1] - color_intensity, 0),
                 max(WALL_COLOR[2] - color_intensity, 0))
        pygame.draw.rect(screen, color, self.rect)

class Base:
    def __init__(self, x, y, size=WALL_SIZE):
        self.rect = pygame.Rect(x, y, size, size)

    def draw(self, screen):
        pygame.draw.rect(screen, EAGLE_COLOR, self.rect)

class Explosion:
    def __init__(self, x, y, size, duration=400):
        self.x = x
        self.y = y
        self.size = size
        self.surface = pygame.Surface((size, size), pygame.SRCALPHA)
        self.start_time = pygame.time.get_ticks()
        self.duration = duration
        self.colors = [
            (pygame.Color('white'), 255),
            (pygame.Color(255, 255, 0, 200), 200),
            (pygame.Color(255, 165, 0, 100), 100),
            (pygame.Color(165, 42, 42, 50), 50)
        ]

    def draw(self, screen):
        time_passed = pygame.time.get_ticks() - self.start_time
        if time_passed >= self.duration:
            return False

        color_index = int(time_passed / self.duration * len(self.colors))
        color, alpha = self.colors[color_index]
        self.surface.fill((0, 0, 0, 0))
        pygame.draw.circle(self.surface, color, (self.size // 2, self.size // 2), self.size // 2)
        screen.blit(self.surface, (self.x - self.size // 2, self.y - self.size // 2))
        return True


